from pygame import init, Rect
from pygame.event import get
from pygame.display import flip, update
from pygame.locals import K_ESCAPE, KEYDOWN, QUIT, K_r
from pygame.draw import rect
from pygame.time import delay
from collections import deque
from constants import *
from car_class import *
from numpy.random import randint, uniform


def Update_Cars(my_cars, red_right, red_left, red_down, red_up, show=True):
    remove_list = []
    change_list = []
    first_before_light = [None for i in range(len(my_cars))]
    for i, dir_list in enumerate(my_cars):
        for j, car in enumerate(dir_list):
            if car.direction == (1,0):
                if car.x < WIDTH//3-SPACE_BETWEEN_CARS:
                    first_before_light[i] = j
                    break
                continue
            elif car.direction == (-1,0):
                if car.x > 2*WIDTH//3:
                    first_before_light[i] = j
                    break
                continue
            elif car.direction == (0,1):
                if car.y < HEIGHT//3-SPACE_BETWEEN_CARS//2:
                    first_before_light[i] = j
                    break
                continue
            else:
                if car.y > 2*HEIGHT//3:
                    first_before_light[i] = j
                    break
                continue

    for i, dir_list in enumerate(my_cars):
        for j, car in enumerate(dir_list):
            # right
            if car.direction == (1,0):
                if j == first_before_light[i] and red_right>=1 and car.x < WIDTH//3-SPACE_BETWEEN_CARS:
                    dist = WIDTH//3-SPACE_BETWEEN_CARS//2-car.x
                    if dist < WIDTH//15:
                        if SPACE_BETWEEN_CARS < dist < 3*SPACE_BETWEEN_CARS:
                            car.decelerate(accel=2*car.acceleration, move_slowly=True)
                        elif dist <= SPACE_BETWEEN_CARS:
                            car.decelerate(front_car_vel=0)
                        elif dist > WIDTH//15:
                            car.decelerate(accel=0.2*car.acceleration, move_slowly=True)
                        else:
                            car.decelerate(move_slowly=True)
                    else:
                        if car.velocity == 0:
                            if uniform() < car.reaction_time:
                                car.accelerate()
                        else:
                            car.accelerate()
                else:
                    if j>0:
                        dist = dir_list[j-1].x-car.x
                    else:
                        dist = WIDTH
                    if dist < WIDTH//20:
                        if SPACE_BETWEEN_CARS < dist < 3*SPACE_BETWEEN_CARS:
                            car.decelerate(accel=2*car.acceleration, move_slowly=True)
                        elif dist <= SPACE_BETWEEN_CARS:
                            car.decelerate(front_car_vel=dir_list[j-1].velocity)
                        elif dist > WIDTH//15:
                            car.decelerate(accel=0.2*car.acceleration, move_slowly=True)
                        else:
                            car.decelerate(move_slowly=True)
                    else:
                        if car.velocity == 0:
                            if uniform() < car.reaction_time and dir_list[j-1].velocity > dir_list[j-1].slow_velocity/2:
                                car.accelerate()
                        else:
                            car.accelerate()

            # left
            elif car.direction == (-1,0):
                if j == first_before_light[i] and red_left>=1 and car.x > 2*WIDTH//3:
                    dist = car.x - 2*WIDTH//3
                    if dist < WIDTH//15:
                        if SPACE_BETWEEN_CARS < dist < 3*SPACE_BETWEEN_CARS:
                            car.decelerate(accel=2*car.acceleration, move_slowly=True)
                        elif dist <= SPACE_BETWEEN_CARS:
                            car.decelerate(front_car_vel=0)
                        elif dist > WIDTH//15:
                            car.decelerate(accel=0.2*car.acceleration, move_slowly=True)
                        else:
                            car.decelerate(move_slowly=True)
                    else:
                        if car.velocity == 0:
                            if uniform() < car.reaction_time:
                                car.accelerate()
                        else:
                            car.accelerate()
                else:
                    if j>0:
                        dist = car.x - dir_list[j-1].x
                    else:
                        dist = WIDTH
                    if dist < WIDTH//20:
                        if SPACE_BETWEEN_CARS < dist < 3*SPACE_BETWEEN_CARS:
                            car.decelerate(accel=2*car.acceleration, move_slowly=True)
                        elif dist <= SPACE_BETWEEN_CARS:
                            car.decelerate(front_car_vel=dir_list[j-1].velocity)
                        elif dist > WIDTH//15:
                            car.decelerate(accel=0.2*car.acceleration, move_slowly=True)
                        else:
                            car.decelerate(move_slowly=True)
                    else:
                        if car.velocity == 0:
                            if uniform() < car.reaction_time and dir_list[j-1].velocity > dir_list[j-1].slow_velocity/2:
                                car.accelerate()
                        else:
                            car.accelerate()

            # up
            elif car.direction == (0,-1):
                if j == first_before_light[i] and car.y > 2*HEIGHT//3:
                    dist = car.y - 2*HEIGHT//3
                    if dist < WIDTH//15 and red_up>=1:
                        if SPACE_BETWEEN_CARS < dist < 3*SPACE_BETWEEN_CARS:
                            car.decelerate(accel=2*car.acceleration, move_slowly=True)
                        elif dist <= SPACE_BETWEEN_CARS:
                            car.decelerate(front_car_vel=0)
                        elif dist > WIDTH//15:
                            car.decelerate(accel=0.5*car.acceleration, move_slowly=True)
                        else:
                            car.decelerate(move_slowly=True)
                    else:
                        if car.velocity == 0:
                            if uniform() < car.reaction_time:
                                car.accelerate()
                        else:
                            car.accelerate()
                else:
                    if j>0:
                        dist = car.y - dir_list[j-1].y
                    else:
                        dist = WIDTH
                    if dist < WIDTH//20:
                        if SPACE_BETWEEN_CARS < dist < 3*SPACE_BETWEEN_CARS:
                            car.decelerate(accel=2*car.acceleration, move_slowly=True)
                        elif dist <= SPACE_BETWEEN_CARS:
                            car.decelerate(front_car_vel=dir_list[j-1].velocity)
                        elif dist > WIDTH//15:
                            car.decelerate(accel=0.5*car.acceleration, move_slowly=True)
                        else:
                            car.decelerate(move_slowly=True)
                    else:
                        if car.velocity == 0:
                            if uniform() < car.reaction_time and dir_list[j-1].velocity > dir_list[j-1].slow_velocity/2:
                                car.accelerate()
                        else:
                            car.accelerate()

            # down
            elif car.direction == (0,1):
                if j == first_before_light[i] and car.y <  HEIGHT//3 - SPACE_BETWEEN_CARS//2:
                    dist = HEIGHT//3 - SPACE_BETWEEN_CARS//2 - car.y
                    if dist < WIDTH//15 and red_down>=1:
                        if SPACE_BETWEEN_CARS < dist < 3*SPACE_BETWEEN_CARS:
                            car.decelerate(accel=2*car.acceleration, move_slowly=True)
                        elif dist <= SPACE_BETWEEN_CARS:
                            car.decelerate(front_car_vel=0)
                        elif dist > WIDTH//15:
                            car.decelerate(accel=0.5*car.acceleration, move_slowly=True)
                        else:
                            car.decelerate(move_slowly=True)
                    else:
                        if car.velocity == 0:
                            if uniform() < car.reaction_time:
                                car.accelerate()
                        else:
                            car.accelerate()
                else:
                    if j>0:
                        dist = dir_list[j-1].y - car.y
                    else:
                        dist = WIDTH
                    if dist < WIDTH//20:
                        if SPACE_BETWEEN_CARS < dist < 3*SPACE_BETWEEN_CARS:
                            car.decelerate(accel=2*car.acceleration, move_slowly=True)
                        elif dist <= SPACE_BETWEEN_CARS:
                            car.decelerate(front_car_vel=dir_list[j-1].velocity)
                        elif dist > WIDTH//15:
                            car.decelerate(accel=0.5*car.acceleration, move_slowly=True)
                        else:
                            car.decelerate(move_slowly=True)
                    else:
                        if car.velocity == 0:
                            if uniform() < car.reaction_time and dir_list[j-1].velocity > dir_list[j-1].slow_velocity/2:
                                car.accelerate()
                        else:
                            car.accelerate()

            car.move()

            if show:
                car.show()

            if car.direction == (1,0):
                if car.x > WIDTH:
                    remove_list.append((i,j))
                elif car.turn_right and car.x == STARTS[5][0]:
                    change_list.append((i,j))
            elif car.direction == (-1,0):
                if car.x < 0:
                    remove_list.append((i,j))
                elif car.turn_right and car.x == STARTS[6][0]:
                    change_list.append((i,j))
            elif car.direction == (0,1):
                if car.y > HEIGHT:
                    remove_list.append((i,j))
            elif car.direction == (0,-1):
                if car.y < 0:
                    remove_list.append((i,j))
    for i,j in remove_list:
        my_cars[i].pop(j)

    for i,j in change_list:
        if my_cars[i][j].direction == (1,0) and i == 1:
            temp_x = my_cars[i][j].x
            temp_y = my_cars[i][j].y
            future_list = my_cars[i][j].future_list
            insertion = first_before_light[future_list] if first_before_light[future_list]!= None else 0
            my_cars[i].pop(j)
            my_cars[future_list].insert(insertion, Car(temp_x, temp_y, STARTS[future_list][2], screen=screen))
        elif my_cars[i][j].direction == (-1,0) and i == 2:
            temp_x = my_cars[i][j].x
            temp_y = my_cars[i][j].y
            future_list = my_cars[i][j].future_list
            insertion = first_before_light[future_list] if first_before_light[future_list]!= None else 0
            my_cars[i].pop(j)
            my_cars[future_list].insert(insertion, Car(temp_x, temp_y, STARTS[future_list][2], screen=screen))

    return my_cars


def draw_background():
    # Roads
    rect(screen, BLACK, Rect((0, HEIGHT//3), (WIDTH, HEIGHT//3)))
    rect(screen, BLACK, Rect((WIDTH//3, 0), (WIDTH//3, HEIGHT)))

    # Double Yellow Lines
    rect(screen, YELLOW_OFF, Rect((0, HEIGHT//2-HEIGHT//150), (WIDTH//3, HEIGHT//200)))
    rect(screen, YELLOW_OFF, Rect((0, HEIGHT//2+HEIGHT//150), (WIDTH//3, HEIGHT//200)))
    rect(screen, YELLOW_OFF, Rect(((2*WIDTH)//3, HEIGHT//2-HEIGHT//150), (WIDTH//3, HEIGHT//200)))
    rect(screen, YELLOW_OFF, Rect(((2*WIDTH)//3, HEIGHT//2+HEIGHT//150), (WIDTH//3, HEIGHT//200)))

    rect(screen, YELLOW_OFF, Rect((WIDTH//2-HEIGHT//150, 0), (HEIGHT//200, HEIGHT//3)))
    rect(screen, YELLOW_OFF, Rect((WIDTH//2+HEIGHT//150, 0), (HEIGHT//200, HEIGHT//3)))
    rect(screen, YELLOW_OFF, Rect((WIDTH//2-HEIGHT//150, (2*HEIGHT)//3), (HEIGHT//200, HEIGHT//3)))
    rect(screen, YELLOW_OFF, Rect((WIDTH//2+HEIGHT//150, (2*HEIGHT)//3), (HEIGHT//200, HEIGHT//3)))

    # White Lines for Red Light
    rect(screen, WHITE, Rect((WIDTH//3, HEIGHT//3-HEIGHT//100), (WIDTH//3, HEIGHT//100)))
    rect(screen, WHITE, Rect((WIDTH//3, (2*HEIGHT)//3), (WIDTH//3, HEIGHT//100)))
    rect(screen, WHITE, Rect(((2*WIDTH)//3, HEIGHT//3), (HEIGHT//100, HEIGHT//3)))
    rect(screen, WHITE, Rect((WIDTH//3-HEIGHT//100, HEIGHT//3), (HEIGHT//100, HEIGHT//3)))

    # Dashed Lines for Lanes
    for i in range(10):
        rect(screen, WHITE, Rect((2*i*(WIDTH//60), 5*HEIGHT//12-HEIGHT//200), (WIDTH//60, HEIGHT//200)))
        rect(screen, WHITE, Rect((2*i*(WIDTH//60), 7*HEIGHT//12-HEIGHT//200), (WIDTH//60, HEIGHT//200)))
        rect(screen, WHITE, Rect((2*WIDTH//3 + (2*i+1)*(WIDTH//60), 5*HEIGHT//12-HEIGHT//200), (WIDTH//60, HEIGHT//200)))
        rect(screen, WHITE, Rect((2*WIDTH//3 + (2*i+1)*(WIDTH//60), 7*HEIGHT//12-HEIGHT//200), (WIDTH//60, HEIGHT//200)))
        if i <= 5:
            rect(screen, WHITE, Rect((5*WIDTH//12-HEIGHT//200, 2*i*(WIDTH//60)), (HEIGHT//200, WIDTH//60)))
            rect(screen, WHITE, Rect((7*WIDTH//12+HEIGHT//200, 2*i*(WIDTH//60)), (HEIGHT//200, WIDTH//60)))
            rect(screen, WHITE, Rect((5*WIDTH//12-HEIGHT//200, 2*HEIGHT//3 + (2*i+1)*(WIDTH//60)), (HEIGHT//200, WIDTH//60)))
            rect(screen, WHITE, Rect((7*WIDTH//12+HEIGHT//200, 2*HEIGHT//3 + (2*i+1)*(WIDTH//60)), (HEIGHT//200, WIDTH//60)))

    rect(screen, WHITE, Rect((19*(WIDTH//60), 7*HEIGHT//12-HEIGHT//200), (WIDTH//60, HEIGHT//200)))
    rect(screen, WHITE, Rect((2*WIDTH//3, 5*HEIGHT//12-HEIGHT//200), (WIDTH//60, HEIGHT//200)))

    rect(screen, WHITE, Rect((5*WIDTH//12-HEIGHT//200, 11*(WIDTH//60)), (HEIGHT//200, WIDTH//60)))
    rect(screen, WHITE, Rect((7*WIDTH//12+HEIGHT//200, 2*HEIGHT//3), (HEIGHT//200, WIDTH//60)))


def Make_Play_Space():
    init()
    # Initialize background
    global screen
    screen = set_mode((WIDTH, HEIGHT))
    screen.fill(GRAY)
    draw_background()
    flip()
    return


if __name__ == '__main__':

    new_car_freq = 15 # how often we allow a new car to (probably) spawn
    main_road_spawn_prob = 0.2 # prob that a lane on the main road spawns a car
    side_road_penality = 0.2 # scale the main road's spawn probability
    main_road_right_turn = 0.2 # probability that a main road car turns right

    my_cars = [[] for _ in range(len(STARTS))]

    Make_Play_Space()
    running = True
    time_step = 0
    red_right = 2   # 0:Green, 1:Yellow, 2:Red
    red_left = 2    # 0:Green, 1:Yellow, 2:Red
    red_down = 2    # 0:Green, 1:Yellow, 2:Red
    red_up = 2      # 0:Green, 1:Yellow, 2:Red
    # red_right = 0   # 0:Green, 1:Yellow, 2:Red
    # red_left = 0    # 0:Green, 1:Yellow, 2:Red
    # red_down = 0    # 0:Green, 1:Yellow, 2:Red
    # red_up = 0      # 0:Green, 1:Yellow, 2:Red
    while running:
        delay(1000//161)
        for event in get():
            if event.type == QUIT:
                running = False
                break
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    running = False
                    break
                elif event.key == K_r:
                    breakpoint()

        # cycle through red lights
        time_step = (time_step+1) % 1500
        if time_step < 100:
            red_up = 2
            red_down = 2
        elif time_step < 1000:
            red_right = 0
            red_left = 0
        elif time_step < 1100:
            red_right = 2
            red_left = 2
        else:
            red_up = 0
            red_down = 0

        if time_step % new_car_freq == 0:
            if uniform() < main_road_spawn_prob:
                my_cars[0].append(Car(*STARTS[0], screen=screen))
            if uniform() < main_road_spawn_prob:
                if uniform() < main_road_right_turn:
                    my_cars[1].append(Car(*STARTS[1], screen=screen, turn_right=True))
                else:
                    my_cars[1].append(Car(*STARTS[1], screen=screen))
            if uniform() < main_road_spawn_prob:
                if uniform() < main_road_right_turn:
                    my_cars[2].append(Car(*STARTS[2], screen=screen, turn_right=True))
                else:
                    my_cars[2].append(Car(*STARTS[2], screen=screen))
            if uniform() < main_road_spawn_prob:
                my_cars[3].append(Car(*STARTS[3], screen=screen))
            if uniform() < main_road_spawn_prob*side_road_penality:
                my_cars[4].append(Car(*STARTS[4], screen=screen))
            if uniform() < main_road_spawn_prob*side_road_penality:
                my_cars[5].append(Car(*STARTS[5], screen=screen))
            if uniform() < main_road_spawn_prob*side_road_penality:
                my_cars[6].append(Car(*STARTS[6], screen=screen))
            if uniform() < main_road_spawn_prob*side_road_penality:
                my_cars[7].append(Car(*STARTS[7], screen=screen))




        draw_background()
        Update_Cars(my_cars, red_right, red_left, red_down, red_up, show=True)
        flip()
