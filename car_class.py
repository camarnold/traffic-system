from pygame.sprite import Sprite
from pygame import Surface
from numpy.random import normal
from numpy import pi, sin, cos
from constants import *


# Define a class for our pixels/grid spaces
class Car(Sprite):
    def __init__(self, x, y, direction, screen=None, turn_right=False):

        super(Car, self).__init__()

        self.x = x
        self.y = y
        self.width = CAR_SIZE
        self.height = CAR_SIZE
        self.state = 0
        self.color = WHITE

        self.direction = direction
        driver_speed = max(normal(loc=0, scale=0.9), -1) # can't be slower than grandma
        if driver_speed < -0.3:
            self.reaction_time = 0.02
        elif driver_speed < 0.5:
            self.reaction_time = 0.05
        elif driver_speed < 1:
            self.reaction_time = 0.1
        else:
            self.reaction_time = 1
        self.acceleration = .2 + driver_speed/10
        self.velocity = 0.1
        self.max_velocity = 4 + driver_speed
        self.slow_velocity = 1 + driver_speed/2
        self.time = 0
        self.angle = 0
        self.turn_right = turn_right
        self.flag = False

        if screen != None:
            self.screen = screen
            self.surf = Surface((CAR_SIZE, CAR_SIZE))
            self.surf.fill(self.color)
            self.rect = self.surf.get_rect()

        if turn_right:
            if self.x == STARTS[1][0]:
                self.future_list = 5
                self.radius = WIDTH//24
                self.center = (9*WIDTH//24-self.radius, 15*HEIGHT//24-CAR_SIZE//2 + self.radius)
                self.circumference = 2*pi*self.radius/4
                self.angle = pi/2
            elif self.x == STARTS[2][0]:
                self.future_list = 6
                self.radius = WIDTH//24
                self.center = (15*WIDTH//24+self.radius, 9*HEIGHT//24-CAR_SIZE//2 - self.radius)
                self.circumference = 2*pi*self.radius/4
                self.angle = 3*pi/2


    def accelerate(self, front_car_vel=None, accel=None, move_slowly=False, kinda_slowly=False):
        if not move_slowly:
            if accel != None:
                if front_car_vel == None:
                    self.velocity = max(min(self.max_velocity, self.velocity+accel), 0)
                else:
                    self.velocity = max(min(front_car_vel, self.max_velocity, self.velocity+accel), 0)
            else:
                if front_car_vel == None:
                    self.velocity = min(self.max_velocity, self.velocity+self.acceleration)
                else:
                    self.velocity = min(front_car_vel, self.max_velocity, self.velocity+self.acceleration)
        if not move_slowly:
            if accel != None:
                if front_car_vel == None:
                    self.velocity = max(min(self.max_velocity, self.velocity+accel), 0)
                else:
                    self.velocity = max(min(front_car_vel, self.max_velocity, self.velocity+accel), 0)
            else:
                if front_car_vel == None:
                    self.velocity = min(self.max_velocity, self.velocity+self.acceleration)
                else:
                    self.velocity = min(front_car_vel, self.max_velocity, self.velocity+self.acceleration)

        else:
            if front_car_vel == None:
                self.velocity = max(min(self.max_velocity, self.velocity+self.acceleration), self.slow_velocity)
            else:
                self.velocity = max(min(front_car_vel, self.max_velocity, self.velocity+self.acceleration), self.slow_velocity)


    def decelerate(self, front_car_vel=None, accel=None, move_slowly=False):
        if not move_slowly:
            if accel != None:
                if front_car_vel == None:
                    self.velocity = max(min(self.max_velocity, self.velocity-accel), 0)
                else:
                    self.velocity = max(min(front_car_vel, self.max_velocity, self.velocity-accel), 0)
            else:
                if front_car_vel == None:
                    self.velocity = max(min(self.max_velocity, self.velocity-self.acceleration), 0)
                else:
                    self.velocity = max(min(front_car_vel, self.max_velocity, self.velocity-self.acceleration), 0)
        else:
            if front_car_vel == None:
                self.velocity = max(min(self.max_velocity, self.velocity-self.acceleration), self.slow_velocity)
            else:
                self.velocity = max(min(front_car_vel, self.max_velocity, self.velocity-self.acceleration), self.slow_velocity)


    def move(self):
        if self.flag:
            if self.turn_right and self.direction==(1,0):
                del_angle = (self.velocity/self.circumference)*pi/2
                self.angle = max(self.angle-del_angle, 0)
                self.x = min(self.center[0] + self.radius*cos(self.angle), STARTS[self.future_list][0])
                self.y = self.center[1] - self.radius*sin(self.angle)
            elif self.turn_right and self.direction==(-1,0):
                del_angle = (self.velocity/self.circumference)*pi/2
                self.angle = max(self.angle-del_angle, pi)
                self.x = max(self.center[0] + self.radius*cos(self.angle), STARTS[self.future_list][0])
                self.y = self.center[1] - self.radius*sin(self.angle)

        else:
            self.x += self.direction[0]*self.velocity
            self.y += self.direction[1]*self.velocity
            if self.turn_right and self.direction==(1,0):
                if self.x > self.center[0]:
                    self.flag = True
                    dist = self.x-self.center[0]
                    del_angle = (dist/self.circumference)*pi/2
                    self.angle -= del_angle
                    self.x = self.center[0] + self.radius*cos(self.angle)
                    self.y = self.center[1] - self.radius*sin(self.angle)
            elif self.turn_right and self.direction==(-1,0):
                if self.x < self.center[0]:
                    self.flag = True
                    dist = self.x-self.center[0]
                    del_angle = (dist/self.circumference)*pi/2
                    self.angle -= del_angle
                    self.x = self.center[0] + self.radius*cos(self.angle)
                    self.y = self.center[1] - self.radius*sin(self.angle)





    def show(self):
        self.rect = self.surf.get_rect()
        self.screen.blit(self.surf, (self.x, self.y))
