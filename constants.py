from pygame.display import set_mode

# Define constants
WIDTH = 1500
HEIGHT = 900
CAR_SIZE = 8
SPACE_BETWEEN_CARS = 16

# Define colors
WHITE = (255,255,255) # cars
BLACK = (0,0,0) # road
GREEN = (0,255,0) # light
GREEN_OFF = (0,255//2,0) # light off
YELLOW = (255,255,0) # light
YELLOW_OFF = (255//2,255//2,0) # light off
RED = (255,0,0) # light
RED_OFF = (255//2,0,0) # light off
GRAY = (255//2,255//2,255//2) # offroad




# where we can start
STARTS = [
            (-CAR_SIZE, 13*HEIGHT//24-CAR_SIZE//2, (1,0)), # going right top lane
            (-CAR_SIZE, 15*HEIGHT//24-CAR_SIZE//2, (1,0)), # going right bot lane
            (WIDTH+CAR_SIZE, 9*HEIGHT//24-CAR_SIZE//2, (-1,0)), # going left top lane
            (WIDTH+CAR_SIZE, 11*HEIGHT//24-CAR_SIZE//2, (-1,0)), # going left bot lane
            (11*WIDTH//24, -CAR_SIZE, (0,1)), # going down right lane
            (9*WIDTH//24, -CAR_SIZE, (0,1)), # going down left lane
            (15*WIDTH//24, HEIGHT+CAR_SIZE, (0,-1)), # going up right lane
            (13*WIDTH//24, HEIGHT+CAR_SIZE, (0,-1)) # going up left lane
         ]
