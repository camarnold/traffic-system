from tensorflow import keras
import tensorflow as tf
import numpy as np

LEARNING_RATE       = 2.5e-4
GAMMA               = 0.99
GAE_LAMBDA          = 0.95
PPO_EPSILON         = 0.2
CRITIC_DISCOUNT     = 1.
ENTROPY_BETA        = 0.01
PPO_STEPS           = 1024
MINI_BATCH_SIZE     = 64
PPO_EPOCHS          = 10
TEST_FREQ           = 20
NUM_TESTS           = 10
TARGET_REWARD       = 300

MAX_GRAD_NORM       = 0.5

def normalize(x):
    x -= np.mean(x)
    x /= (np.std(x) + 1e-8)
    return x


def compute_gae(next_value, rewards, masks, values, gamma=GAMMA, lam=GAE_LAMBDA):
    values = values + [next_value]
    gae = 0
    returns = []
    for step in reversed(range(len(rewards))):
        delta = rewards[step] + (gamma * values[step + 1] * masks[step] - values[step]) # r+td_error
        gae = delta + gamma * lam * masks[step] * gae
        # prepend to get correct order back
        returns.insert(0, gae + values[step])
    return returns


def test_env(env, model, render=False, deterministic=False):
    state = env.reset()
    done = False
    total_reward = 0
    if render:
        env.render()
    while not done:
        state = state.reshape((1,-1))
        dist, _ = model.call(state)
        if deterministic:
            action = dist.mean()
        else:
            action = dist.sample()
        action = tf.clip_by_value(action, env.action_space.low, env.action_space.high)
        state, reward, done, _ = env.step(action.numpy().squeeze())
        total_reward += reward
        if render:
            env.render()
    env.close()
    return total_reward


class ActorCriticModel(keras.Model):
    def __init__(self, state_shape, num_actions, min_std=1e-5):
        super(ActorCriticModel, self).__init__()

        self.common1 = layers.Dense(256, input_shape=(None, *state_shape), activation='swish')   # allow any number of inputs at once
        self.common2 = layers.Dense(128, activation='swish')

        self.critic_dense2 = layers.Dense(128, activation='relu')
        self.critic_dense3 = layers.Dense(64, activation='relu')
        self.critic_output = layers.Dense(1)

        self.actor_dense2 = layers.Dense(128, activation='relu')
        self.actor_dense3 = layers.Dense(64, activation='relu')
        self.actor_mean = layers.Dense(num_actions)
        self.actor_std = layers.Dense(num_actions, activation='relu')

        # https://github.com/openai/baselines/blob/ea25b9e8b234e6ee1bca43083f8f3cf974143998/baselines/ppo2/model.py#L100
        self.train_op = keras.optimizers.Adam(learning_rate=LEARNING_RATE, epsilon=1e-5)

        self.min_std = min_std


    def call(self, inputs):#, deterministic=False):
        inputs = tf.convert_to_tensor(inputs)
        common = self.common1(inputs)
        common1 = self.common2(common)

        act = self.actor_dense2(common1)
        act2 = self.actor_dense3(act)
        self.mu  = self.actor_mean(act2)
        self.std = self.actor_std(act2) + self.min_std  # std = 0 ==> entropy = NaN
        dist = tfp.distributions.Normal(self.mu, self.std)

        crit = self.critic_dense2(common1)
        crit2 = self.critic_dense3(crit)
        value = self.critic_output(crit2)

        return dist, value


    def train(self, total_steps, states, actions, log_probs, returns, advantages, clip_param=PPO_EPSILON):
        count_steps = 0
        sum_returns = 0.0
        sum_advantage = 0.0
        sum_l_clip = 0.0
        sum_l_vf = 0.0
        sum_entropy = 0.0
        sum_loss_total = 0.0

        # PPO EPOCHS is the number of times we will go through ALL the training data to make updates
        for _ in range(PPO_EPOCHS):
            # grabs random mini-batches several times until we have covered all data
            for state, action, old_log_probs, return_, advantage in self._ppo_iterator(states, actions, log_probs, returns, advantages):

                with tf.GradientTape() as tape:
                    tape.watch(self.trainable_variables)   # keep track of the trainable variables (don't always need all of them)
                    loss, l_clip, l_vf, entropy = self._get_loss(state, action, old_log_probs, return_, advantage, clip_param)
                    grads = tape.gradient(loss, self.trainable_variables)  # get_gradients (backprop) from losses

                # https://github.com/openai/baselines/blob/ea25b9e8b234e6ee1bca43083f8f3cf974143998/baselines/ppo2/model.py#L102-L108
                grads, _ = tf.clip_by_global_norm(grads, MAX_GRAD_NORM)
                self.train_op.apply_gradients(zip(grads, self.trainable_variables))  # change weights based on gradients

                # track statistics
                sum_returns += np.mean(return_)
                sum_advantage += np.mean(advantage)
                sum_l_clip += l_clip
                sum_l_vf += l_vf
                sum_loss_total += loss
                sum_entropy += entropy

                count_steps += 1


    def _ppo_iterator(self, states, actions, log_probs, returns, advantage):
        batch_size = len(states)
        # generates random mini-batches until we have covered the full batch
        for _ in range(len(states) // MINI_BATCH_SIZE):
            rand_ids = tf.convert_to_tensor(np.random.randint(0, batch_size, MINI_BATCH_SIZE))
            yield tf.gather(states, rand_ids), tf.gather(actions, rand_ids), \
                  tf.gather(log_probs, rand_ids), tf.gather(returns, rand_ids), tf.gather(advantage, rand_ids)


    def _get_loss(self, state, action, old_log_probs, return_, advantage, clip_param):

        advantage = normalize(advantage) # https://github.com/openai/baselines/blob/ea25b9e8b234e6ee1bca43083f8f3cf974143998/baselines/ppo2/model.py#L139

        dist, value_ = self.call(state)

        new_log_probs = dist.log_prob(action)
        ratio = tf.exp(new_log_probs - old_log_probs)
        surr1 = ratio * advantage
        surr2 = tf.clip_by_value(ratio, 1.0 - clip_param, 1.0 + clip_param) * advantage
        l_clip = tf.reduce_mean(tf.minimum(surr1, surr2), axis=1)

        l_vf = tf.reduce_mean(tf.pow(return_ - value_, 2), axis=1)
        entropy = self._get_entropy(dist)   # how confident we are in our actions

        loss = -(l_clip - CRITIC_DISCOUNT*l_vf + ENTROPY_BETA*entropy)

        return loss, l_clip, l_vf, entropy


    def _get_entropy(self, distribution):
        return tf.reduce_mean(distribution.entropy(), axis=1)
